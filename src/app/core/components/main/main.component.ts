import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Component } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { selectCartItemCount } from '../../../pages/cart/store/cart.selectors';
import { selectLoading } from '../../../common/store/core.selectors';
const { version } = require('../../../../../package.json')
import { WebComponentWrapperOptions } from '@angular-architects/module-federation-tools';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss']
})
export class MainComponent {
  loading$: Observable<boolean> = this.store.select(selectLoading);
  cartCount$: Observable<any> = this.store.select(selectCartItemCount);
  version: string = version;
  footer: WebComponentWrapperOptions = {
    remoteEntry: 'https://lib.debugger.pl/remoteEntry.js',
    remoteName: 'lib',
    exposedModule: './footer',
    elementName: 'ui-footer',
  }

  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
    .pipe(
      map(result => result.matches)
    );

  constructor(
    private breakpointObserver: BreakpointObserver,
    private store: Store
  ) { }

}

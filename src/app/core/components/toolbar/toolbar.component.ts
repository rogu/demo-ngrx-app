import { Component, Input } from '@angular/core';
import { CoreService } from '../../core.service';

@Component({
  selector: 'app-toolbar',
  templateUrl: './toolbar.component.html'
})
export class ToolbarComponent {

  @Input() cartCount!: number;

  constructor(public utilsService: CoreService) { }

}

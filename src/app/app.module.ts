import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { EffectsModule } from '@ngrx/effects';
import * as fromRouter from '@ngrx/router-store';
import { StoreRouterConnectingModule } from '@ngrx/router-store';
import { StoreModule } from '@ngrx/store';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CoreModule } from './core/core.module';
import { CartIDBService } from './pages/cart/services/cart.idb.service';
import { CartService } from './pages/cart/services/cart.ls.service';
import * as fromStore from './pages/cart/store';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    HttpClientModule,
    CoreModule.forRoot({ toolbarTitle: 'NgRx App' }),
    StoreModule.forRoot(
      { router: fromRouter.routerReducer },
      {
        runtimeChecks: {
          strictStateImmutability: true,
          strictActionImmutability: true
        }
      }
    ),
    EffectsModule.forRoot([]),
    StoreModule.forFeature('cart', fromStore.cartReducer),
    EffectsModule.forFeature([fromStore.CartEffects]),
    StoreRouterConnectingModule.forRoot(),
    StoreDevtoolsModule.instrument()
  ],
  bootstrap: [AppComponent],
  providers: [
    { provide: CartService, useClass: CartIDBService }
  ]
})
export class AppModule { }

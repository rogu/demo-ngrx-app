import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MatBadgeModule } from '@angular/material/badge';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatIconModule } from '@angular/material/icon';
import { MatTableModule } from '@angular/material/table';
import { RouterModule } from '@angular/router';
import { DataGridModule } from 'demo-ng-kolekto';
import { CartComponent } from './components/cart/cart.component';
import { InfoComponent } from './components/info.component';

@NgModule({
  imports: [
    CommonModule,
    MatTableModule,
    MatCardModule,
    MatIconModule,
    MatBadgeModule,
    MatButtonModule,
    MatCheckboxModule,
    DataGridModule.forRoot({ bgColor: 'lightcyan' }),
    RouterModule.forChild([
      { path: '', component: CartComponent }
    ])
  ],
  declarations: [
    CartComponent,
    InfoComponent
  ],
  exports: [
    CartComponent
  ]
})
export class CartModule { }

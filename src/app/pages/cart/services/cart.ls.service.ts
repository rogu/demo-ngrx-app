import { Injectable } from '@angular/core';
import { CartItemModel } from '../store/cart.model';
export const LOCALSTORAGE_KEY = 'NG_ADV_APP_CART';

@Injectable({
  providedIn: 'root'
})
export class CartService {

  constructor() { }

  getStorage(): Promise<CartItemModel[]> {
    return Promise.resolve(JSON.parse(localStorage.getItem(LOCALSTORAGE_KEY) || ''));
  }

  updateStorage(state: any): void {
    localStorage.setItem(LOCALSTORAGE_KEY, JSON.stringify(state));
  }
}

import { tap, filter, take } from 'rxjs/operators';
import { Store } from '@ngrx/store';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import * as fromStore from "./store";
import { selectCartLoaded } from './store/cart.selectors';

@Injectable({
  providedIn: 'root'
})
export class CartGuard {
  constructor(private store: Store) { }

  canActivate(): Observable<boolean> {
    return this.store.select(selectCartLoaded)
      .pipe(
        tap((data) => !data && this.store.dispatch(fromStore.loadCart())),
        filter((data) => !!data),
        take(1)
      )
  }
}

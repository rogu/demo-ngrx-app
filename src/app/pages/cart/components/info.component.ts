import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
    template: `
        you bought items:
        <app-data-grid [data]="data" [headers]="['LP','TITLE','COUNT']">
            <ng-template let-item let-lp="nr">
                <td>{{lp}}</td>
                <td>{{item.title}}</td>
                <td>{{item.count}}</td>
            </ng-template>
        </app-data-grid>
    `
})

export class InfoComponent implements OnInit {
    constructor(
        @Inject(MAT_DIALOG_DATA) public data: any,
    ) { }

    ngOnInit() { }
}

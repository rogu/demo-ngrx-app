import { CartItemModel } from './../../store/cart.model';
import { Observable } from 'rxjs';
import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import * as CartStore from '../../store';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html'
})
export class CartComponent implements OnInit {
  cart$: Observable<any> = this.store.select(CartStore.selectAll);
  itemsToBuy$: Observable<any> = this.store.select(CartStore.selectCartIdsToSend);

  constructor(private store: Store) { }

  ngOnInit() { }

  add(item: CartItemModel) {
    this.store.dispatch(CartStore.addToCart({ item }));
  }

  remove(item: CartItemModel) {
    this.store.dispatch(CartStore.removeFromCart({ item }));
  }

  suspend(suspend: boolean, { id }: CartItemModel) {
    this.store.dispatch(CartStore.updateCartItem({ id, changes: { suspend } }));
  }

  buy() {
    this.store.dispatch(CartStore.buyCartItems());
  }
}

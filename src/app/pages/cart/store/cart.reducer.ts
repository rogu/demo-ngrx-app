import * as CartActions from './cart.actions';
import { CartItemModel, CartState } from './cart.model';
import { ActionReducerMap, createReducer, on } from '@ngrx/store';
import { createEntityAdapter, EntityAdapter } from "@ngrx/entity";
import { ItemModel } from '../../items/store/items.models';

export const cartAdapter: EntityAdapter<CartItemModel> = createEntityAdapter<CartItemModel>();

export const state: CartState = cartAdapter.getInitialState({
  loaded: false,
  loading: true
});

const reducer = createReducer(
  state,
  on(CartActions.loadCartSuccess, (state, { data }) =>
    cartAdapter.setAll(data, { ...state, loaded: true, loading: false })
  ),
  on(CartActions.addToCart, (state, { item }: { item: any }) => {
    const found = state.entities[item.id];
    return cartAdapter.upsertOne({ ...item, count: found ? (found.count + 1) : 1 }, { ...state, loading: true });
  }),
  on(CartActions.removeFromCart, (state, { item }: any) => item.count === 1
    ? cartAdapter.removeOne(item.id, { ...state, loading: true })
    : cartAdapter.updateOne({ id: item.id, changes: { count: item.count - 1 } }, { ...state, loading: true })
  ),
  on(CartActions.updateCartItem, (state, item: any) => cartAdapter.updateOne(item, { ...state, loading: true })),
  on(CartActions.buyCartItemsSuccess, (state, { data }) => cartAdapter.removeMany(data, { ...state })),
  on(CartActions.updateStorageSuccess, (state) => ({ ...state, loading: false }))
);

export const cartReducer: ActionReducerMap<unknown, CartActions.types> = reducer;


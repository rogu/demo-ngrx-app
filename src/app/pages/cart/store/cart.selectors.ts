import { createFeatureSelector, createSelector } from '@ngrx/store';
import { CartState } from './cart.model';
import { cartAdapter } from './cart.reducer';

export const cartState = createFeatureSelector<CartState>('cart');
export const { selectAll, selectIds, selectEntities } = cartAdapter.getSelectors(cartState);

export const selectCartLoaded = createSelector(
  cartState,
  (state) => state.loaded
);

export const selectCartItemCount = createSelector(
  selectAll,
  (state) => state?.reduce((acc, item) => acc + item.count, 0)
);

export const selectCartLoading = createSelector(
  cartState,
  (state) => state?.loading
);

export const selectCartIdsToSend = createSelector(
  selectIds,
  selectEntities,
  (state: any[], entities) => state.filter((id: string) => !entities[id]?.suspend)
)

export const selectCartItemsToSend = createSelector(
  selectCartIdsToSend,
  selectEntities,
  (ids, entities) => ids.map((id: string) => entities[id])
)

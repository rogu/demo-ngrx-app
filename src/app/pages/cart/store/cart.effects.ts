import { Injectable } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import { iif, of } from 'rxjs';
import { delay, map, switchMap, tap, withLatestFrom } from 'rxjs/operators';
import { ItemsService } from '../../items/services/items.service';
import { ItemModel } from '../../items/store/items.models';
import { InfoComponent } from '../components/info.component';
import { CartService } from '../services/cart.ls.service';
import * as CartActions from './cart.actions';
import * as CartSelectors from './cart.selectors';
import { CartItemModel } from './cart.model';

@Injectable()
export class CartEffects {

  constructor(
    private actions$: Actions,
    private cartService: CartService,
    private itemsService: ItemsService,
    private store: Store,
    private dialog: MatDialog
  ) { }

  loadCart$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(CartActions.loadCart),
      switchMap(async () => await this.cartService.getStorage() || []),
      switchMap((data: CartItemModel[]) => iif(() => !!data.length,
        of(data),
        this.itemsService.getMandatory().pipe(
          map(({ data }): CartItemModel[] => data.map((item: ItemModel) => ({ ...item, count: 1 }))),
          tap((data) => this.cartService.updateStorage(data))
        )
      )),
      map((data) => CartActions.loadCartSuccess({ data }))
    )
  });

  updateCart$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(CartActions.addToCart, CartActions.removeFromCart, CartActions.updateCartItem, CartActions.buyCartItems, CartActions.buyCartItemsSuccess),
      withLatestFrom(this.store.select(CartSelectors.selectAll)),
      tap(([, data]) => this.cartService.updateStorage(data)),
      delay(1000), // wydłużenie czasu w celu wyświetlania spinnera.
      map(_ => CartActions.updateStorageSuccess())
    )
  });

  sendCart$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(CartActions.buyCartItems),
      withLatestFrom(this.store.select(CartSelectors.selectCartItemsToSend), this.store.select(CartSelectors.selectCartIdsToSend)),
      tap(([, items]) => {
        // send items to server
        this.dialog.open(InfoComponent, { data: items })
      }),
      map(([, , ids]) => CartActions.buyCartItemsSuccess({ data: ids }))
    )
  });

}

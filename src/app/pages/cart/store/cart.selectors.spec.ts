import { TestScheduler } from 'rxjs/testing';
import { selectCartItemCount } from './cart.selectors';
import { merge } from 'rxjs';

fdescribe('Cart Selectors', () => {

  let scheduler: any = null;

  beforeEach(() => {
    scheduler = new TestScheduler((actual, expected) => {
      expect(actual).toEqual(expected);
    });
  })

  it('should select items count', () => {
    const cartItems: Partial<any>[] = [
      { count: 5 },
      { count: 10 }
    ];

    expect(selectCartItemCount.projector(cartItems)).toEqual(15);
  });

  it('easy observable testing', () => {
    scheduler.run((helpers: any) => {
      const { cold, expectObservable } = helpers;
      const source$ = cold('-a-|', { a: 'foo' });
      //source$.subscribe(console.log, null, () => console.log('finito'))
      const expected$ = '-x-|';
      expectObservable(source$).toBe(expected$, { x: 'foo' });
    });
  });

  it('merge testing', () => {
    scheduler.run((helpers: any) => {
      const { expectObservable, hot } = helpers;
      const e1 = hot('----a-|');
      const e2 = hot('-d-|');
      const expected = '-d--a-|';
      expectObservable(merge(e1, e2)).toBe(expected);
    });
  })

});

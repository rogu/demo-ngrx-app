import { createAction, union, props } from '@ngrx/store';
import { ItemModel } from '../../items/store/items.models';
import { CartItemModel, UpdateItem } from './cart.model';

export const loadCart = createAction(
    '[Cart] Load'
);

export const loadCartSuccess = createAction(
    '[Cart] Load Success',
    props<{ data: CartItemModel[] }>()
);

export const addToCart = createAction(
    '[Cart] Add',
    props<{ item: ItemModel | CartItemModel }>()
);

export const removeFromCart = createAction(
    '[Cart] Remove',
    props<{ item: CartItemModel }>()
);

export const updateCartItem = createAction(
    '[Cart] Update Cart Item',
    props<UpdateItem<CartItemModel>>()
);

export const updateStorageSuccess = createAction(
    '[Cart] Update Storage Success'
);

export const buyCartItems = createAction(
    '[Cart] Buy Cart Items'
);

export const buyCartItemsSuccess = createAction(
    '[Cart] Buy Cart Items Success',
    props<{ data: any }>()
);

const types = union({ loadCart, loadCartSuccess, addToCart, removeFromCart, updateStorageSuccess });
export type types = typeof types;

import { ItemModel } from "../../../pages/items/store/items.models";
import { EntityState } from '@ngrx/entity';

export interface CartItemModel extends ItemModel {
    count: number;
    suspend: boolean;
}

export interface CartState extends EntityState<CartItemModel> {
    readonly loaded: boolean;
    readonly loading: boolean;
}


export interface UpdateItem<T> {
    id: number;
    changes: Partial<T>;
}

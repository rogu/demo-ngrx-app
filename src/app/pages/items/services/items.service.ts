import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from "rxjs";
import { Api } from 'src/app/common/utils/api';

@Injectable({ providedIn: 'root' })
export class ItemsService {
    constructor(
        private http: HttpClient
    ) { }

    fetch(): Observable<any> {
        return this.http.get(Api.ITEMS_END_POINT);
    }

    getMandatory(): Observable<any> {
        return this.http.get(Api.MANDATORY_END_POINT);
    }
}

import { Injectable } from '@angular/core';
import { CanActivate } from '@angular/router';
import { Store } from '@ngrx/store';
import { tap, filter, mapTo } from 'rxjs/operators';
import * as ItemsStore from '../store';
import { Observable } from 'rxjs';
import { ItemModel } from '../store/items.models';

@Injectable()
export class ItemsGuard implements CanActivate {

  constructor(private store: Store) { }

  canActivate(): Observable<boolean> {
    return this.store.select(ItemsStore.selectAll).pipe(
      tap(({ length }: ItemModel[]) => !length && this.store.dispatch(ItemsStore.FetchItems())),
      filter(({ length }: ItemModel[]): boolean => !!length),
      mapTo(true)
    )
  }

}

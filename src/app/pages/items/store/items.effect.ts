import { Injectable } from '@angular/core';
import { Actions, ofType, createEffect } from '@ngrx/effects';
import { map, switchMapTo } from 'rxjs/operators';
import { ItemsService } from '../services/items.service';
import { FetchItemsSuccess, FetchItems } from './items.actions';

@Injectable()
export class ItemsEffects {
  constructor(
    private actions$: Actions,
    private itemsService: ItemsService
  ) { }

  fetch$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(FetchItems),
      switchMapTo(this.itemsService.fetch()),
      map((resp) => FetchItemsSuccess(resp))
    )
  });
}

import { ItemModel, ItemsState } from "./items.models";
import * as ItemsActions from './items.actions';
import { on, createReducer } from '@ngrx/store';
import { createEntityAdapter, EntityAdapter } from "@ngrx/entity";

export const itemsAdapter: EntityAdapter<ItemModel> = createEntityAdapter<ItemModel>()

export const state: ItemsState = itemsAdapter.getInitialState({
    loading: false
});

const reducer = createReducer(
    state,
    on(ItemsActions.FetchItems, state => ({ ...state, loading: true })),
    on(ItemsActions.FetchItemsSuccess, (state, { data }) =>
        itemsAdapter.setAll(data, { ...state, loading: false }))
);

export const itemsReducer = reducer;

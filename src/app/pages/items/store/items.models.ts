import { EntityState } from "@ngrx/entity";

export interface ItemModel {
    id: number;
    category: string;
    imgSrc: string;
    price: number;
    title: string;
}

export type ItemsState = EntityState<ItemModel> & { loading: boolean; };

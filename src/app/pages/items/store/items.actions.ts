import { createAction, union, props } from '@ngrx/store';
import { ItemModel } from './items.models';

export const FetchItems = createAction("[Items] Fetch");

export const FetchItemsSuccess = createAction(
    "[Items] Fetch Success",
    props<{ data: ItemModel[] }>()
);

const types = union({ FetchItems, FetchItemsSuccess });
export type types = typeof types;

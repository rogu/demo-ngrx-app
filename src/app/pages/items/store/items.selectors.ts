import { EntityState } from "@ngrx/entity";
import { getRouterSelectors } from '@ngrx/router-store';
import { createFeatureSelector, createSelector } from "@ngrx/store";
import { ItemModel } from './items.models';
import { itemsAdapter } from "./items.reducer";

const { selectCurrentRoute } = getRouterSelectors();

export const selectItemsState = createFeatureSelector<EntityState<ItemModel>>('items');

export const {selectAll} = itemsAdapter.getSelectors(selectItemsState);

export const selectSelected = createSelector(
  selectAll,
  selectCurrentRoute,
  (state, router: any) => state.find((item: ItemModel) => item.id === router.params.id)
)

export const selectItemsLoading = createSelector(
  selectItemsState,
  (state: any) => state?.loading
);

import { CartItemModel } from './../../../cart/store/cart.model';
import { Component } from '@angular/core';
import { Store } from '@ngrx/store';
import * as CartStore from './../../../../pages/cart/store';
import { Observable } from 'rxjs';
import { ItemModel } from '../../store/items.models';
import * as ItemsStore from '../../store'

@Component({
  selector: 'app-items',
  templateUrl: 'items.component.html'
})
export class ItemsComponent {

  items$: Observable<ItemModel[]> = this.store.select(ItemsStore.selectAll);

  constructor(private store: Store) { }

  buy(item: CartItemModel) {
    this.store.dispatch(CartStore.addToCart({ item }));
  }

}

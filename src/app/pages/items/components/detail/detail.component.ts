import { Component } from '@angular/core';
import { Observable } from 'rxjs';
import { Store } from '@ngrx/store';
import { selectSelected } from '../../store/items.selectors';


@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.scss']
})
export class DetailComponent {
  data$: Observable<any> = this.store.select(selectSelected);

  constructor(private store: Store) { }

}

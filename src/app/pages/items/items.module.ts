import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { DetailComponent } from "./components/detail/detail.component";
import { RouterModule } from "@angular/router";
import { MatCardModule } from "@angular/material/card";
import { MatIconModule } from "@angular/material/icon";
import { MatPaginatorModule } from "@angular/material/paginator";
import { MatSortModule } from "@angular/material/sort";
import { MatTableModule } from "@angular/material/table";
import { HttpClientModule } from "@angular/common/http";
import { StoreModule } from "@ngrx/store";
import { itemsReducer } from "./store/items.reducer";
import { EffectsModule } from "@ngrx/effects";
import { ItemsEffects } from "./store/items.effect";
import { ItemsGuard } from "./services/items.guard";
import { ItemsService } from "./services/items.service";
import { ItemsComponent } from "./components/items/items.component";
import { MatButtonModule } from "@angular/material/button";
import { DataGridModule } from "demo-ng-kolekto";

@NgModule({
  declarations: [DetailComponent, ItemsComponent],
  imports: [
    CommonModule,
    MatTableModule,
    MatIconModule,
    MatPaginatorModule,
    MatSortModule,
    MatCardModule,
    MatIconModule,
    MatButtonModule,
    HttpClientModule,
    DataGridModule.forRoot({ bgColor: '#fbfbfb' }),
    StoreModule.forFeature("items", itemsReducer),
    EffectsModule.forFeature([ItemsEffects]),
    RouterModule.forChild([
      {
        path: "",
        component: ItemsComponent,
        canActivate: [ItemsGuard],
        children: [{ path: ":id", component: DetailComponent }],
      },
    ]),
  ],
  providers: [ItemsService, ItemsGuard]
})
export class ItemsModule { }

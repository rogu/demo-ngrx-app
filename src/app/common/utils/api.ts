import { environment } from "src/environments/environment";

export class Api {
    static BASE_END_POINT = environment.BASE_END_POINT;
    static ITEMS_END_POINT = Api.BASE_END_POINT + 'items';
    static MANDATORY_END_POINT = Api.ITEMS_END_POINT + '/mandatory';
    static FORM_CONFIG_END_POINT = Api.BASE_END_POINT + 'utils/register-config-form';
}

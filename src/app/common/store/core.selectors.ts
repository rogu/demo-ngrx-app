import { createSelector } from '@ngrx/store';
import { selectItemsLoading } from '../../pages/items/store/items.selectors';
import { selectCartLoading } from '../../pages/cart/store/cart.selectors';

export const selectLoading = createSelector(
    selectCartLoading,
    selectItemsLoading,
    (cart: boolean, items: boolean) => cart || items
)

import { CartGuard } from './pages/cart/cart.guard';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuardService } from './core/auth-guard.service';
import { InitComponent } from './core/components/init/init.component';
import { NotFoundComponent } from './core/components/not-found/not-found.component';

const routes: Routes = [
  {
    path: '', canActivate: [AuthGuardService, CartGuard], children: [
      { path: '', component: InitComponent },
      { path: 'items', loadChildren: () => import('./pages/items/items.module').then(m => m.ItemsModule) },
      { path: 'cart', loadChildren: () => import('./pages/cart/cart.module').then(m => m.CartModule) },
      { path: '**', component: NotFoundComponent }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

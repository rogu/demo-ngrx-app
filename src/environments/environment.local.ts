export const environment = {
  production: false,
  BASE_END_POINT: 'https://api.debugger.pl/',
  WS_END_POINT: '//localhost:2323/',
  WS_PREFIX: 'ws:'
};
